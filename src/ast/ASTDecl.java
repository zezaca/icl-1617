package ast;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import util.Binding;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTDecl implements ASTNode {
	
	Binding decl;
	ASTNode expr;

    public ASTDecl(Binding decl, ASTNode expr)
    {
		this.decl = decl; 
		this.expr = expr;
    }

    public String toString() {
    		String s = "";
    		s += decl.getId() + " = " + decl.getExpr().toString();
    		return "decl " + s + " in " + expr.toString() + " end";
    }
    
	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		int value;

		int idValue = decl.getExpr().eval(env);

		Environment<Integer> newEnv = env.beginScope();		
		newEnv.assoc(decl.getId(), idValue);
		value = expr.eval(newEnv);
		newEnv.endScope();
	
		return value;
	}

	@Override
	public void compile(CodeBlock code) {
		// create frame
		StackFrame frame = code.createFrame();
		decl.getExpr().compile(code);
		// store value
		// begin scope (push SP)
		code.pushFrame(frame);
		// associate id to address
		expr.compile(code);
		// end scope (pop SP)
		code.popFrame();
	}
}

