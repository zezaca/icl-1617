package util;
import ast.ASTNode;

public class Binding {
	
	private String id;
	private ASTNode expr;

	public Binding(String id, ASTNode expr) {
		this.id = id;
		this.expr = expr;
	}

	public String getId() {
		return this.id;
	}

	public ASTNode getExpr() {
		return expr;
	}
}

